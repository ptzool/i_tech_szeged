provider "openstack" {
    user_name  = "admin"
    tenant_name = "admin"
    password  = "a05e20cc5a734708"
    auth_url  = "http://10.20.0.5:5000/v2.0"
}

resource "openstack_compute_keypair_v2" "openacademy_key" {
  name = "openacademy_key"
  public_key = "${file("key/id_rsa.pub")}"
  region = "${var.region}"
}

resource "openstack_compute_secgroup_v2" "openacademy_group" {
  name = "openacademy_group"
  description = "openacademy Demo Group"
  region = "${var.region}"

  rule {
    from_port = 22
    to_port = 22
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }

  rule {
    from_port = 80
    to_port = 80
    ip_protocol = "tcp"
    cidr = "0.0.0.0/0"
  }
}

resource "openstack_compute_floatingip_v2" "openacademy_floating_ip_1" {
  pool = "external_network"
}

resource "openstack_compute_floatingip_v2" "openacademy_floating_ip_2" {
  pool = "external_network"
}

resource "openstack_compute_floatingip_v2" "openacademy_floating_ip_3" {
  pool = "external_network"
}

resource "openstack_compute_instance_v2" "openacademy_instance_1" {
  name = "openacademy_instance_1"
  image_id = "${var.image_id}"
  flavor_name = "${var.flavor_name}"
  key_pair = "${openstack_compute_keypair_v2.openacademy_key.name}"
  security_groups = [ "default", "all", "${openstack_compute_secgroup_v2.openacademy_group.name}" ]
  region = "${var.region}"

  connection {
    user = "fedora"
    key_file = "key/id_rsa"
  }

  provisioner file {
    source = "variables.tf"
    destination = "variables.tf"
  }
  
  network {
    uuid = "083f2c22-16c2-40a1-8a6d-a3c16228eaab"
  }
  
  floating_ip = "${openstack_compute_floatingip_v2.openacademy_floating_ip_1.address}"
}

resource "openstack_compute_instance_v2" "openacademy_instance_2" {
  name = "openacademy_instance_2"
  image_id = "${var.image_id}"
  flavor_name = "${var.flavor_name}"
  key_pair = "${openstack_compute_keypair_v2.openacademy_key.name}"
  security_groups = [ "default", "all", "${openstack_compute_secgroup_v2.openacademy_group.name}" ]
  region = "${var.region}"

  connection {
    user = "fedora"
    key_file = "key/id_rsa"
  }

  provisioner file {
    source = "variables.tf"
    destination = "variables.tf"
  }
  
  network {
    uuid = "083f2c22-16c2-40a1-8a6d-a3c16228eaab"
  }
  
  floating_ip = "${openstack_compute_floatingip_v2.openacademy_floating_ip_2.address}"
}

resource "openstack_compute_instance_v2" "openacademy_instance_3" {
  name = "openacademy_instance_3"
  image_id = "${var.image_id}"
  flavor_name = "${var.flavor_name}"
  key_pair = "${openstack_compute_keypair_v2.openacademy_key.name}"
  security_groups = [ "default", "all", "${openstack_compute_secgroup_v2.openacademy_group.name}" ]
  region = "${var.region}"

  connection {
    user = "fedora"
    key_file = "key/id_rsa"
  }

  provisioner file {
    source = "variables.tf"
    destination = "variables.tf"
  }
  
  network {
    uuid = "083f2c22-16c2-40a1-8a6d-a3c16228eaab"
  }
  
  floating_ip = "${openstack_compute_floatingip_v2.openacademy_floating_ip_3.address}"
}



