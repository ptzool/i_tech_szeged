variable "image_id" {
  default = "97693aed-46a6-4210-8fe7-a3d7f8c1dde6"
}

variable "flavor_name" {
  default = "m1.medium"
}

variable "region" {
  default = "RegionOne"
}
